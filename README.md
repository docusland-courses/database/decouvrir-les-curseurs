### Les curseurs

Partons de la structure et de la data présente dans le projet : https://github.com/Docusland/database-procedures

https://dev.mysql.com/doc/refman/5.7/en/cursors.html

Ce qui requiert la compréhension de la gestion des erreurs 

```sql
DROP PROCEDURE test_condition2;
DELIMITER |
CREATE PROCEDURE test_condition2(IN p_ville VARCHAR(100))
BEGIN
    DECLARE v_nom, v_prenom VARCHAR(100);
    
    -- On déclare fin comme un BOOLEAN, avec FALSE pour défaut
    DECLARE fin BOOLEAN DEFAULT FALSE;                     
    
    DECLARE curs_clients CURSOR
        FOR SELECT nom, prenom
        FROM Client
        WHERE ville = p_ville;

    -- On utilise TRUE au lieu de 1
    DECLARE CONTINUE HANDLER FOR NOT FOUND SET fin = TRUE; 

    OPEN curs_clients;                                    

    loop_curseur: LOOP                                                
        FETCH curs_clients INTO v_nom, v_prenom;

        IF fin THEN     -- Plus besoin de "= 1"
            LEAVE loop_curseur;
        END IF;
                   
        SELECT CONCAT(v_prenom, ' ', v_nom) AS 'Client';
    END LOOP;

    CLOSE curs_clients; 
END|
DELIMITER ;

CALL test_condition2('Houtsiplou');
CALL test_condition2('Bruxelles');
```


#### Exercice Cursors:

Faire le script ci-dessus avec la boucle WHILE ou encore REPEAT
Réaliser un boucle listant les musiciens du groupe 24